import os
import time
import random
import pyperclip
import sys
class Gen:
	def __init__(self, name, key, app, byear):
		self.name = name
		self.key = key
		self.app = app
		self.byear = byear
		self.Generator()
	def Generator(self):
		name = self.name
		key = self.key
		app = self.app
		byear = self.byear
		year = int(byear[0])
		if year == 1:
			year = '@'
		elif year == 2:
			year = '!'
		else:
			year = self.byear[0]
		self.crypted = name[2].upper() + app[3] + byear[1] + key[1] + app[-2] + name[-2] + byear[2] + app[-0].upper() + key[-3].upper() + year
		print 'Do you want to copy your pass to your clipboard? '
		chs = raw_input('y/n: ')
		if chs == 'y':
			pyperclip.setcb(self.crypted)
			time.sleep(1)
			print 'Pasted to your clipboard!'
			raw_input()
			exit()
		if chs == 'n':
			print 'Your pass is: ', self.crypted
			raw_input()
			exit()
		else:
			print 'Choose a valid choice'
			self.Generator()

def Info():
	app = raw_input('Name of service you are requesting a password for: ')
	appS = app.split()
	if len(appS) < 1 or len(appS) > 1:
		print 'Too many arguments'
		Info()
	else:
		if len(app) < 4 or len(app) > 12:
			print 'Service name too long or too short'
			Info()
		else:
			name = raw_input('Your name: ')
			nameS = name.split()
			if len(nameS) < 1 or len(nameS) > 1:
				print 'Too many arguments'
				Info()
			else:
				if len(name) < 3 or len(name) > 9:
					print 'Name too long or too short'
					Info()
				if ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] in nameS or ['!', '"', "'", '#', '$', '%', '&', '/', '(', ')', '=', '{', '}', '[', ']'] in nameS:
					print 'Names arent supoused to have letter in them'
					Info()
				else:
					byear = raw_input('Year you were born: ')
					byearS = name.split()
					if len(byearS) < 1 or len(byearS) > 1:
						print 'Too many arguments'
						Info()
					else:
						if len(byear) < 4 or len(byear) > 4:
							print 'Year only are 4 digits'
							Info()
						else:
							key = raw_input('Private Key: ')
							keyS = key.split()
							if len(key) < 6 or len(key) > 16:
								print 'Key must have at least 6 digits and maximum 16'
							else:
								appL = app.lower()
								nameL = name.lower()
								obj = Gen(nameL, key, appL, byear)
Info()
