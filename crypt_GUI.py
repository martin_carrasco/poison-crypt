import os
import time
import Tkinter
import random
import pyperclip
import sys
from Tkinter import *
import tkMessageBox
class Crypter(Frame):
	def createWidgets(self):
		self.grid()
		self.Quit = Button(self, text='Exit', fg='black', command=self.quit)
		self.Quit.grid(column=0, row=5, columnspan=5)
#		GEN
		self.gen = Button(self, text='Generate', fg='black', command=self.Checker)
		self.gen.grid(column=0, row=4, columnspan=4)
#		SERVICE
		self.servI = StringVar()
		self.serv = Entry(self, fg='black', textvariable=self.servI)
		self.serv.grid(column=1, row=0)
		self.serv.bind('<Return>', self.Checker)
		self.servM = Label(self, text='Service:', fg='black')
		self.servM.grid(column=0, row=0)
#		NAME
		self.nmeI = StringVar()
		self.nme = Entry(self, fg='black', textvariable=self.nmeI)
		self.nme.grid(column=1, row=1)
		self.nme.bind('<Return>', self.Checker)
		self.nmeM = Label(self, text='Name:', fg='black')
		self.nmeM.grid(column=0, row=1)
#		YEAR
		self.yearI = StringVar()
		self.year = Entry(self, fg='black', textvariable=self.yearI)
		self.year.grid(column=1, row=2)		
		self.year.bind('<Return>', self.Checker)
		self.yearM = Label(self, text='Birthday Year:', fg='black')
		self.yearM.grid(column=0, row=2)
#		KEY
		self.pkeyI = StringVar()
		self.pkey = Entry(self, fg='black', textvariable=self.pkeyI, show='*')
		self.pkey.grid(column=1, row=3)
		self.pkey.bind('<Return>', self.Checker)
		self.pkeyM = Label(self, text='Key:', fg='black')
		self.pkeyM.grid(column=0, row=3)
#########################################

	def Checker(self, event):
		servS = self.servI.get().split()
		if len(servS) < 1 or len(servS) > 1:
			tkMessageBox.showinfo('Error', 'Service has too many arguments or too few!')
			self.servI.set('')
		else:
			if len(self.servI.get()) < 4 or len(self.servI.get()) > 12:
				tkMessageBox.showinfo('Error', 'Service needs to be between 4 and 12 characters!')
				self.servI.set('')
			else:
				nameS = self.nmeI.get().split()
				if len(nameS) < 1 or len(nameS) > 1:
					tkMessageBox.showinfo('Error', 'Name has too many arguments or too few!')
					self.nmeI.set('')
				else:
					if len(self.nmeI.get()) < 3 or len(self.nmeI.get()) > 9:
						tkMessageBox.showinfo('Error', 'Name must be between 3 and 9 characters')
						self.nmeI.set('')
					if ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] in nameS or ['!', '"', "'", '#', '$', '%', '&', '/', '(', ')', '=', '{', '}', '[', ']'] in nameS:
						tkMessageBox.showinfo('Error', 'Names can only have letters inside them!')
						self.nmeI.set('')
					else:
						byearS = self.yearI.get().split()
						if len(byearS) < 1 or len(byearS) > 1:
							tkMessageBox.showinfo('Error', 'Too many arguments in year or too few!')
							self.yearI.set('')
						else:
							if len(self.yearI.get()) < 4 or len(self.yearI.get()) > 4:
								tkMessageBox.showinfo('Error', 'Years only have 4 letters')
								self.yearI.set('')
							else:
								self.numbs = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
								for x in self.yearI.get():
									if x not in self.numbs:
										tkMessageBox.showinfo('Error', 'There were non-integer characters in the year')
										self.yearI.set('')
								else:		
									if len(self.pkeyI.get()) < 6 or len(self.pkeyI.get()) > 16:
										tkMessageBox.showinfo('Error', 'Key must have between 6 to 16 characters')
										self.yearI.set('')
									else:
										keyS = self.pkey.get().split()
										if len(keyS) < 1 or len(keyS) > 1:
											tkMessageBox.showinfo('Error', 'Too many arguments in key or too few')
											self.yearI.set('')
										else:
											self.App = self.servI.get().lower()
											self.Key = self.pkeyI.get()
											self.Name = self.nmeI.get().lower()
											self.Byear = self.yearI.get()
											self.Crypt(self.App, self.Name, self.Byear, self.Key)
	def Crypt(self, app, name, byear, key):
		year = int(byear[0])
		if year == 1:
			year = '@'
		elif year == 2:
			year = '!'
		else:
			year = self.byear[0]
		self.crypted = name[2].upper() + app[3] + byear[1] + key[1] + app[-2] + name[-2] + byear[2] + app[-0].upper() + key[-3].upper() + year
		self.result = tkMessageBox.askyesno('Info', 'Do you want to copy your pass to clipboard?')
		if self.result == True:
			pyperclip.setcb(self.crypted)
			tkMessageBox.showinfo('Warning', 'Program will close')
			exit()
		else:
			tkMessageBox.showinfo('Info', 'Your password is: ' + self.crypted)
			exit()
		
	def __init__(self, master=None):
		Frame.__init__(self, master)
		self.pack()
		self.createWidgets()
tst = Crypter(master=Tk())
tst.master.title('Poison Pass Gen')
tst.master.maxsize(10000, 400)
tst.mainloop()
